<?php
namespace Magento\Customer\Model\Account\Redirect;

/**
 * Interceptor class for @see \Magento\Customer\Model\Account\Redirect
 */
class Interceptor extends \Magento\Customer\Model\Account\Redirect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\RequestInterface $request, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\UrlInterface $url, \Magento\Framework\Url\DecoderInterface $urlDecoder, \Magento\Customer\Model\Url $customerUrl, \Magento\Framework\Controller\ResultFactory $resultFactory, \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory, ?\Magento\Framework\Url\HostChecker $hostChecker = null)
    {
        $this->___init();
        parent::__construct($request, $customerSession, $scopeConfig, $storeManager, $url, $urlDecoder, $customerUrl, $resultFactory, $cookieMetadataFactory, $hostChecker);
    }

    /**
     * {@inheritdoc}
     */
    public function getRedirect()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRedirect');
        return $pluginInfo ? $this->___callPlugins('getRedirect', func_get_args(), $pluginInfo) : parent::getRedirect();
    }

    /**
     * {@inheritdoc}
     */
    public function setCookieManager($value)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCookieManager');
        return $pluginInfo ? $this->___callPlugins('setCookieManager', func_get_args(), $pluginInfo) : parent::setCookieManager($value);
    }

    /**
     * {@inheritdoc}
     */
    public function getRedirectCookie()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRedirectCookie');
        return $pluginInfo ? $this->___callPlugins('getRedirectCookie', func_get_args(), $pluginInfo) : parent::getRedirectCookie();
    }

    /**
     * {@inheritdoc}
     */
    public function setRedirectCookie($route)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setRedirectCookie');
        return $pluginInfo ? $this->___callPlugins('setRedirectCookie', func_get_args(), $pluginInfo) : parent::setRedirectCookie($route);
    }

    /**
     * {@inheritdoc}
     */
    public function clearRedirectCookie()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'clearRedirectCookie');
        return $pluginInfo ? $this->___callPlugins('clearRedirectCookie', func_get_args(), $pluginInfo) : parent::clearRedirectCookie();
    }
}

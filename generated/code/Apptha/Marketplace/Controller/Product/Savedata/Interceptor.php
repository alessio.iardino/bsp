<?php
namespace Apptha\Marketplace\Controller\Product\Savedata;

/**
 * Interceptor class for @see \Apptha\Marketplace\Controller\Product\Savedata
 */
class Interceptor extends \Apptha\Marketplace\Controller\Product\Savedata implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \Magento\Catalog\Model\ProductFactory $productFactory, \Apptha\Marketplace\Helper\System $systemHelper, \Apptha\Marketplace\Helper\Marketplace $dataHelper, \Magento\Framework\Filesystem\Driver\File $file)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $productRepository, $productFactory, $systemHelper, $dataHelper, $file);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function changeProductData($productData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'changeProductData');
        return $pluginInfo ? $this->___callPlugins('changeProductData', func_get_args(), $pluginInfo) : parent::changeProductData($productData);
    }

    /**
     * {@inheritdoc}
     */
    public function assignProductData($productId, $product, $storeId, $productTypeId, $productData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'assignProductData');
        return $pluginInfo ? $this->___callPlugins('assignProductData', func_get_args(), $pluginInfo) : parent::assignProductData($productId, $product, $storeId, $productTypeId, $productData);
    }

    /**
     * {@inheritdoc}
     */
    public function setProductApproval($productApproval, $product, $productData, $productId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setProductApproval');
        return $pluginInfo ? $this->___callPlugins('setProductApproval', func_get_args(), $pluginInfo) : parent::setProductApproval($productApproval, $product, $productData, $productId);
    }

    /**
     * {@inheritdoc}
     */
    public function setProductData($product, $categoryIds, $productData, $nationalShippingAmount, $internationalShippingAmount, $customAttributes)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setProductData');
        return $pluginInfo ? $this->___callPlugins('setProductData', func_get_args(), $pluginInfo) : parent::setProductData($product, $categoryIds, $productData, $nationalShippingAmount, $internationalShippingAmount, $customAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function saveImageForProduct($productId, $imagesPaths)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'saveImageForProduct');
        return $pluginInfo ? $this->___callPlugins('saveImageForProduct', func_get_args(), $pluginInfo) : parent::saveImageForProduct($productId, $imagesPaths);
    }

    /**
     * {@inheritdoc}
     */
    public function removeImageForProduct($productId, $imagesIds)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'removeImageForProduct');
        return $pluginInfo ? $this->___callPlugins('removeImageForProduct', func_get_args(), $pluginInfo) : parent::removeImageForProduct($productId, $imagesIds);
    }

    /**
     * {@inheritdoc}
     */
    public function baseImageForProduct($productId, $baseImage)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'baseImageForProduct');
        return $pluginInfo ? $this->___callPlugins('baseImageForProduct', func_get_args(), $pluginInfo) : parent::baseImageForProduct($productId, $baseImage);
    }

    /**
     * {@inheritdoc}
     */
    public function updateStockDataForProduct($productId, $productData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateStockDataForProduct');
        return $pluginInfo ? $this->___callPlugins('updateStockDataForProduct', func_get_args(), $pluginInfo) : parent::updateStockDataForProduct($productId, $productData);
    }

    /**
     * {@inheritdoc}
     */
    public function saveCustomOption($productId, $productData)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'saveCustomOption');
        return $pluginInfo ? $this->___callPlugins('saveCustomOption', func_get_args(), $pluginInfo) : parent::saveCustomOption($productId, $productData);
    }

    /**
     * {@inheritdoc}
     */
    public function saveSimpleProductsForConfigurableProduct($simpleProductData, $productAttributeSetId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'saveSimpleProductsForConfigurableProduct');
        return $pluginInfo ? $this->___callPlugins('saveSimpleProductsForConfigurableProduct', func_get_args(), $pluginInfo) : parent::saveSimpleProductsForConfigurableProduct($simpleProductData, $productAttributeSetId);
    }

    /**
     * {@inheritdoc}
     */
    public function getImageInfo($attributeCombinationArray, $configurableProduct, $imagesPaths, $simpleProductImagesPath, $usedPath, $simpleProduct)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getImageInfo');
        return $pluginInfo ? $this->___callPlugins('getImageInfo', func_get_args(), $pluginInfo) : parent::getImageInfo($attributeCombinationArray, $configurableProduct, $imagesPaths, $simpleProductImagesPath, $usedPath, $simpleProduct);
    }

    /**
     * {@inheritdoc}
     */
    public function updateSimpleProductInfo($existingProductId, $simpleProductSku, $qty, $price)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateSimpleProductInfo');
        return $pluginInfo ? $this->___callPlugins('updateSimpleProductInfo', func_get_args(), $pluginInfo) : parent::updateSimpleProductInfo($existingProductId, $simpleProductSku, $qty, $price);
    }

    /**
     * {@inheritdoc}
     */
    public function setQtyValue($existingProductId, $simpleProduct, $configurableQty, $configurableProduct, $attributeCombinationArray)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setQtyValue');
        return $pluginInfo ? $this->___callPlugins('setQtyValue', func_get_args(), $pluginInfo) : parent::setQtyValue($existingProductId, $simpleProduct, $configurableQty, $configurableProduct, $attributeCombinationArray);
    }

    /**
     * {@inheritdoc}
     */
    public function setPriceValue($existingProductId, $simpleProduct, $configurablePrice, $configurableProduct, $attributeCombinationArray)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPriceValue');
        return $pluginInfo ? $this->___callPlugins('setPriceValue', func_get_args(), $pluginInfo) : parent::setPriceValue($existingProductId, $simpleProduct, $configurablePrice, $configurableProduct, $attributeCombinationArray);
    }

    /**
     * {@inheritdoc}
     */
    public function updateImageValueByProduct($currentProductId, $createdProductId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'updateImageValueByProduct');
        return $pluginInfo ? $this->___callPlugins('updateImageValueByProduct', func_get_args(), $pluginInfo) : parent::updateImageValueByProduct($currentProductId, $createdProductId);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteExistingProductImages($deleteImageProductId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'deleteExistingProductImages');
        return $pluginInfo ? $this->___callPlugins('deleteExistingProductImages', func_get_args(), $pluginInfo) : parent::deleteExistingProductImages($deleteImageProductId);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigurableAttributes($selectedOptions, $attributeCombinationArray)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConfigurableAttributes');
        return $pluginInfo ? $this->___callPlugins('getConfigurableAttributes', func_get_args(), $pluginInfo) : parent::getConfigurableAttributes($selectedOptions, $attributeCombinationArray);
    }

    /**
     * {@inheritdoc}
     */
    public function associateSimpleProductsWithConfigurable($action, $productId, $simpleProductData, $simpleProdouctIds, $productAttributeSetId, $editSimpleProductFlag)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'associateSimpleProductsWithConfigurable');
        return $pluginInfo ? $this->___callPlugins('associateSimpleProductsWithConfigurable', func_get_args(), $pluginInfo) : parent::associateSimpleProductsWithConfigurable($action, $productId, $simpleProductData, $simpleProdouctIds, $productAttributeSetId, $editSimpleProductFlag);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        return $pluginInfo ? $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo) : parent::getActionFlag();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        return $pluginInfo ? $this->___callPlugins('getRequest', func_get_args(), $pluginInfo) : parent::getRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        return $pluginInfo ? $this->___callPlugins('getResponse', func_get_args(), $pluginInfo) : parent::getResponse();
    }
}

<?php
namespace Apptha\Marketplace\Controller\Assignproduct\Cart\Add;

/**
 * Interceptor class for @see \Apptha\Marketplace\Controller\Assignproduct\Cart\Add
 */
class Interceptor extends \Apptha\Marketplace\Controller\Assignproduct\Cart\Add implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator, \Magento\Framework\App\Request\Http $request, \Magento\Checkout\Model\Cart $cart)
    {
        $this->___init();
        parent::__construct($context, $scopeConfig, $checkoutSession, $storeManager, $formKeyValidator, $request, $cart);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function checkQuoteError($product)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'checkQuoteError');
        return $pluginInfo ? $this->___callPlugins('checkQuoteError', func_get_args(), $pluginInfo) : parent::checkQuoteError($product);
    }

    /**
     * {@inheritdoc}
     */
    public function checkingForSellerProduct($params)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'checkingForSellerProduct');
        return $pluginInfo ? $this->___callPlugins('checkingForSellerProduct', func_get_args(), $pluginInfo) : parent::checkingForSellerProduct($params);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductStockData($productId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductStockData');
        return $pluginInfo ? $this->___callPlugins('getProductStockData', func_get_args(), $pluginInfo) : parent::getProductStockData($productId);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        return $pluginInfo ? $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo) : parent::getActionFlag();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        return $pluginInfo ? $this->___callPlugins('getRequest', func_get_args(), $pluginInfo) : parent::getRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        return $pluginInfo ? $this->___callPlugins('getResponse', func_get_args(), $pluginInfo) : parent::getResponse();
    }
}
